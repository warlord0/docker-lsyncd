#!/bin/sh

TARGET_HOST=${TARGET_HOST:-root@remote}
TARGET_DIR=${TARGET_DIR:-data}
TARGET_PORT=${TARGET_PORT:-9022}

cat <<EOF >/etc/lsyncd.conf
settings {
  logfile = "/dev/stdout",
  statusFile = "/var/run/lsyncd.status",
  pidfile = "/var/run/lsyncd.pid",
  nodaemon = "true"
}
sync {
  default.rsyncssh,
  source = '/mnt/data',
  host = '${TARGET_HOST}',
  targetdir = '${TARGET_DIR}',
  ssh = {
    port = ${TARGET_PORT}
  }
}
EOF

exec /usr/bin/lsyncd -nodaemon -delay 0 /etc/lsyncd.conf